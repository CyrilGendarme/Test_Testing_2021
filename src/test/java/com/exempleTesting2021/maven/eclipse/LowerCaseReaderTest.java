package com.exempleTesting2021.maven.eclipse;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import p_exe1.Chargeur;
import p_exe1.MyAdapter;
import p_exe1.TelephoneType1;
import p_exe1.TelephoneType2;
import p_exe2.LowerCaseReader;

public class LowerCaseReaderTest {
	
	private static Reader r;
	private static LowerCaseReader reader;
	private static char ch[];

    @BeforeClass
    public static void initCalculator() throws FileNotFoundException { 
    	r = new FileReader("testout.txt"); 
		reader = new LowerCaseReader(r); 
		ch = new char[5]; 
    }
 
  
  
    @Test
    public void lectureChaine() {
    	System.out.print("Lecture d'une chaine de 5 char : "); 
        for (int i = 0; i < 5; i++)  System.out.print(ch[i]); 
    }
 
    
    @Test//(expected = Exception.class)
    public void lectureParCaractere() throws IOException  {
    	System.out.print("\n\nLecture de 4 char : "); 
        for (int i = 0; i <4 ; i++) System.out.print("\nchar n" + i + " : " + (char)reader.read() ); 
    }
	
	
}
