package com.exempleTesting2021.maven.eclipse;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import calculator_exemple.Calculator;
import p_exe1.Chargeur;
import p_exe1.MyAdapter;
import p_exe1.TelephoneType1;
import p_exe1.TelephoneType2;

public class ChargeurTelephoneTEST {

	private static TelephoneType1 tel1; 
	private static TelephoneType2 tel2;
	private static Chargeur chargeur;
	private static MyAdapter adapter; 
	

    @BeforeClass
    public static void initCalculator() {
    	tel1 = new TelephoneType1();
    	tel2 = new TelephoneType2();
    	chargeur = new Chargeur();
    	adapter = new MyAdapter(tel2);
    }
 
 
    @Test
    public void testTelDeBase() {
    	chargeur.BrancherTel(tel1);	
    }
 
    @Test
    public void testAvecAdaptateur() {
    	chargeur.BrancherTel(adapter);
    }
	
}
