package com.exempleTesting2021.maven.eclipse;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.Reader;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import p_exe2.LowerCaseReader;
import p_exe3.IRepertoire;
import p_exe3.ProxyRepertoire;

public class ProxyRepertoireTest {

	private static ByteArrayOutputStream outContent;
	private static IRepertoire rep1;
	private static IRepertoire rep2;

    @BeforeClass
    public static void initCalculator() throws FileNotFoundException { 
    	rep1 = new ProxyRepertoire(System.getProperty("user.name")) ;
		rep2 = new ProxyRepertoire("John") ;
		outContent = new ByteArrayOutputStream();
		System.setOut(new PrintStream(outContent));
    }
    
    @After
    public void afterEachTest() throws IOException {
    	outContent.reset();
    }
 
    @Test
    public void testBonUser() throws IOException { 
    	rep1.lister();
    	assertEquals("Lister pour le repertoire de root !\n", outContent.toString());
    }
  
    
    @Test
    public void testMauvaisUser() throws IOException  {
    	rep2.lister();
    	assertEquals("Mauvais proprio ! \n ", outContent.toString());
    }
	
    
    
}
