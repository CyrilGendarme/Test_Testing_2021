package p_exe3;

public class ProxyRepertoire implements IRepertoire {

	private String proprio;
	private IRepertoire myRep;
	
	public ProxyRepertoire(String prop) {
		this.proprio = prop;
		this.myRep = new Repertoire(prop);
	}

	@Override
	public void lister() {
		if (proprio.equals(System.getProperty("user.name")))
			myRep.lister();
		else System.out.print("Mauvais proprio ! \n ");
	}

}


