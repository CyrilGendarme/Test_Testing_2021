package p_exe3;

import java.io.IOException;

public class TestClass {
	
	public static void main(String[] args) throws IOException {
		
		IRepertoire rep1 = new ProxyRepertoire(System.getProperty("user.name")) ;
		IRepertoire rep2 = new ProxyRepertoire("John") ;
		rep1.lister();
		rep2.lister();
	}

}


