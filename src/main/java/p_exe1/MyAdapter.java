package p_exe1;

public class MyAdapter implements ChargeableIF{

	private TelephoneType2 myTel;
	
	public MyAdapter(TelephoneType2 tel) {
		super();
		myTel = tel;
	}

	public void charge() {
		System.out.println("Nous passons bien par la methode de l'adapter ");
		myTel.chargePhone();
	}
}


