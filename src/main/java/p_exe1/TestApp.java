package p_exe1;

public class TestApp {

	public static void main(String[] args) {

		TelephoneType1 tel1 = new TelephoneType1();
		TelephoneType2 tel2 = new TelephoneType2();
		Chargeur chargeur = new Chargeur();
		MyAdapter adapter = new MyAdapter(tel2);
		
		chargeur.BrancherTel(tel1);
		chargeur.BrancherTel(adapter);
	}
}

