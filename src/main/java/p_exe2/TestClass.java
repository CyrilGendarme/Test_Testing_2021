package p_exe2;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class TestClass {

	public static void main(String[] args) throws IOException {
		
		Reader r = new FileReader("testout.txt"); 
		LowerCaseReader reader = new LowerCaseReader(r);  
		char ch[] = new char[5]; 
		reader.read(ch, 0, 5); 
		
		System.out.print("Lecture d'une chaine de 5 char : "); 
        for (int i = 0; i < 5; i++)  System.out.print(ch[i]); 
         
        System.out.print("\n\nLecture de 4 char : "); 
        for (int i = 0; i <4 ; i++) System.out.print("\nchar n" + i + " : " + (char)reader.read() ); 
		 
        reader.close();
	}
}


