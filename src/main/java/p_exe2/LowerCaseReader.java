package p_exe2;

import java.io.FilterReader;
import java.io.IOException;
import java.io.Reader;

public class LowerCaseReader extends FilterReader{

	Reader myReader;
	
	public LowerCaseReader(Reader in) {
		super(in);
		myReader = in;
	}

	public int read() throws IOException{
		int i = super.read(); 
		if (i>= 65 || i <=90) i += 32;   // si char majuscule, on se "d�cale" dans le tableau des valeurs ASCII
		return i;
	}
	 
	 public int read(char[] b, int offset, int len) throws IOException {
		 int res = super.read(b, offset, len);
	     for (int i = 0; i < len; i++) 	b[i] = Character.toLowerCase(b[i]);   
	     return res;
	    }
}




